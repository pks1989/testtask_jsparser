(function() {

var PeriodsList = document.querySelectorAll(".work_experience > .period");
var CompanyList = document.querySelectorAll(".work_experience > .info > .meta > .company_name");

for (var i = 0; i < CompanyList.length; ++i) {
  console.log(getOneFormatedWorkExprnss(CompanyList[i],PeriodsList[i]));
}

function getOneFormatedWorkExprnss(Company, Periods) {
  var CompanyName = Company.innerHTML.replace(/<.*?>/g, "");

  var WorkPeriod = Periods.innerHTML.replace(/<.*?>/g, "");
  WorkPeriod = WorkPeriod.split("— ");

  var begin = textToMonthWorked(WorkPeriod[0]);
  var end = textToMonthWorked(WorkPeriod[1]);

  var total= parseInt((end-begin) / 12, 10);
  var output = CompanyName + " - " + total + " " + rightRussianYearWord(total);
  
  return output;
}

function textToMonthWorked (Text){
  if(Text.search(/настоящее/) != -1) return monthWorkedNow();

  Text=Text.split(/ +/);
  var MonthWorked=+Text[1]*12 + textMonthToNumber(Text[0]) + 1;

  return MonthWorked;
}

function textMonthToNumber (Text){
  Text=Text.replace(/Январь/g, "1");
  Text=Text.replace(/Февраль/g, "2");
  Text=Text.replace(/Март/g, "3");
  Text=Text.replace(/Апрель/g, "4");
  Text=Text.replace(/Май/g, "5");
  Text=Text.replace(/Июнь/g, "6");
  Text=Text.replace(/Июль/g, "7");
  Text=Text.replace(/Август/g, "8");
  Text=Text.replace(/Сентябрь/g, "9");
  Text=Text.replace(/Октябрь/g, "10");
  Text=Text.replace(/Ноябрь/g, "11");
  Text=Text.replace(/Декабрь/g, "12");

  return +Text;
}

function monthWorkedNow(){
  var WorkNow = new Date();
  var MonthWorked = WorkNow.getMonth() + 1 + WorkNow.getFullYear() * 12;

  return MonthWorked; 
}

function rightRussianYearWord (year) {
  switch (year % 10) {
    case 0:
	  return "лет";
	case 1:
      return "год";
	case 2:
	case 3:
	case 4:
	  return "года";
	default:
	  return "лет";
  }
}

})();
